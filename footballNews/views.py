import time
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils.datastructures import MultiValueDictKeyError
from footballNews.funciones.buscador import search_new


def busqueda(request):
    try:
        q = request.GET["q"]
        q = q.strip()
        if q == "":
            raise MultiValueDictKeyError
        t1 = time.time()
        resultados = search_new(q)
        segundos = time.time() - t1

        paginator = Paginator(resultados, 15)
        try:
            pagina = request.GET["p"]
            resultados_pagina = paginator.page(pagina)
        except (MultiValueDictKeyError, PageNotAnInteger):
            pagina = 1
            resultados_pagina = paginator.page(pagina)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            pagina = paginator.num_pages
            resultados_pagina = paginator.page(pagina)

    except MultiValueDictKeyError:
        return HttpResponseRedirect(reverse('index'))

    return render_to_response('busqueda.html',
                              {'busqueda': q, 'num_resultados': len(resultados), 'resultados': resultados_pagina,
                               'segundos': segundos}, RequestContext(request))