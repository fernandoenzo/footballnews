#encoding:utf-8
from bs4 import BeautifulSoup
from datetime import datetime

import re
from pattern.web import Crawler, strip_javascript
from pattern.web import DEPTH
from pattern.vector import Document
from footballNews.funciones.fechas import fecha_marca, fecha_as, fecha_estadio_deportivo
from footballNews.models import Keyword, Noticia


sites = ["http://www.estadiodeportivo.com/futbol-primera-division/",
         "http://futbol.as.com/futbol/",
         "http://www.marca.com/"]

compiled_sites = [re.compile("^" + site) for site in sites]
search = ["futbol/2014/01", re.compile(r"2014/01/\d\d/futbol"), "futbol-primera-division/2014/01"]


class NewsCrawler(Crawler):
    def crawl(self, method=DEPTH, **kwargs):
        while True:
            link = self.next
            if link is None:
                break
            if link.url in sites:   # Si es una url inicial, siempre se visita
                break
            elif link.url in self.visited:
                self.pop()
                continue
            elif (link.url.find("#") != -1) or (
                len(Noticia.objects.filter(url=link.url))):   # No contiene el símbolo # (anclas) o ya existe
                self.visited[link.url] = True
                self.pop()
                continue
            found = False
            for domain in compiled_sites:   # Pertenece a cierto dominio
                if domain.search(link.url):
                    found = True
                    break
            if not found:
                self.visited[link.url] = True
                self.pop()
                continue
            found = False
            for string in search:           # Contiene ciertas subcadenas
                try:
                    if link.url.find(string) != -1:
                        found = True
                        break
                except:
                    if string.search(link.url):
                        found = True
                        break
            if found:
                break
            self.visited[link.url] = True
            self.pop()
        Crawler.crawl(self, method, **kwargs)

    def visit(self, link, source=None):
        print 'visited:', repr(link.url), 'from:', link.referrer
        source = strip_javascript(source)
        soup = BeautifulSoup(source, "html.parser")

        try:
            if link.url.find("marca.com") != -1:
                cabecera = soup.find_all("h1")[0].text
                fecha = soup.find_all("span", attrs={"class": "fecha"})[0].text.strip()
                fecha = fecha_marca(fecha)
                cuerpo = soup.find_all("div", attrs={"class": "cuerpo_articulo"})[0].text.strip()

            elif link.url.find("as.com") != -1:
                cabecera = soup.find_all("h1", attrs={"class": "art-title"})[0].text.strip()
                fecha = soup.find_all("time", attrs={"class": "s-inb ntc-time"})[0].get("datetime")
                fecha = fecha_as(fecha)
                cuerpo = soup.find_all("div", attrs={"class": "art-txt cf"})[0].text.strip()

            elif link.url.find("estadiodeportivo.com") != -1:
                cabecera = soup.find_all("h1")[0].text.strip()
                hora = soup.find_all("font", attrs={"class": "actualizada"})[0].text.strip()
                fecha = fecha_estadio_deportivo(link.url, hora)
                cuerpo = soup.find_all("div", attrs={"class": "noticia"})[0].text.strip()

            else:
                cabecera = ""
                cuerpo = ""
                fecha = None

        except:
            cabecera = ""
            cuerpo = ""
            fecha = None

        source = cabecera + "\n" + cuerpo
        if (cabecera != "") and (cabecera is not None):
            cabecera = cabecera.replace("&quot;", '"')
            cuerpo = cuerpo.replace("&quot;", '"')
            d = Document(source, threshold=1, language="es")
            keywords = d.keywords(top=15)

            try:
                n = Noticia.objects.create(titular=cabecera, url=link.url, fecha=fecha)
                for word in keywords:
                    k = Keyword.objects.get_or_create(keyword=word[1])[0]
                    n.keywords.add(k)
            except:
                pass

    def fail(self, link):
        print 'failed:', repr(link.url)


def search_new(text):
    """Descompone el input en palabras clave y busca con esas palabras en todas las palabras
    clave almacenadas y en los titulares de las noticias"""
    d = Document(text, threshold=0, language="es")
    keywords = d.keywords(top=15)
    res = {}
    for palabra in keywords:
        word = palabra[1]
        results = Keyword.objects.filter(keyword=word)
        for keyword in results:
            for noticia in keyword.noticias.all():
                res[noticia.url] = (noticia.titular, noticia.fecha)
        results = Noticia.objects.filter(titular__icontains=word)
        for noticia in results:
            res[noticia.url] = (noticia.titular, noticia.fecha)
    items = res.items()
    items.sort(comparator_noticias)
    return items


def comparator_noticias(x, y):
    # Cada "noticia" es en realidad una tupla (url, (titular, fecha))
    if y[1][1] > x[1][1]:
        return 1
    elif x[1][1] == y[1][1]:
        return 0
    elif y[1][1] < x[1][1]:
        return -1


if __name__ == "__main__":

    spider = NewsCrawler(links=sites, delay=5)

    # print search_new("Cristiano")

    while not spider.done:
        spider.crawl(method=DEPTH, cached=False, throttle=5)

