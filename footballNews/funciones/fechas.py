#encoding:utf-8

import re
import pytz
from datetime import datetime


fecha_url = re.compile(r'(\d{4}/\d{2}/\d{2})')
tz = pytz.timezone('Europe/Madrid')


def fecha_marca(fecha):
    fecha = fecha.replace(".", "")
    date = datetime.strptime(fecha, "%d/%m/%y - %H:%M")
    date = tz.localize(date)
    return date


def fecha_as(fecha):
    date = datetime.strptime(fecha, "%Y-%m-%d %H:%M")
    date = tz.localize(date)
    return date


def fecha_estadio_deportivo(url, hora):
    fecha = fecha_url.findall(url)[0]
    fecha += " " + hora
    date = datetime.strptime(fecha, "%Y/%m/%d %H:%M")
    date = tz.localize(date)
    return date

