from django.db.models import *


class Noticia(Model):
    titular = CharField(max_length=45)
    url = URLField(unique=True)
    fecha = DateTimeField(null=True)
    keywords = ManyToManyField('Keyword', related_name="noticias", related_query_name="noticias")

    def __unicode__(self):
        return self.titular + "\n" + self.url


class Keyword(Model):
    keyword = CharField(max_length=15, unique=True)

    def __unicode__(self):
        return self.keyword