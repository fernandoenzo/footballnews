from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.views.generic.base import TemplateView
from ProyectoAII.settings import BASE_DIR

admin.autodiscover()

urlpatterns = patterns(
    '',

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^buscar$', 'footballNews.views.busqueda', name="buscar"),

    url(r'^$', TemplateView.as_view(template_name=BASE_DIR + "/footballNews/templates/index.html"), name="index"),

)
